package com.example.demo.model;


import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "points") 
public class Points {
	
	private Map<String, String> battingpoints;
	private Map<String, String> bowlingpoints;
	private Map<String, String> fieldingpoints;
	private Map<String, String> otherpoints;
	public Map<String, String> getBattingpoints() {
		return battingpoints;
	}
	public void setBattingpoints(Map<String, String> battingpoints) {
		this.battingpoints = battingpoints;
	}
	public Map<String, String> getBowlingpoints() {
		return bowlingpoints;
	}
	public void setBowlingpoints(Map<String, String> bowlingpoints) {
		this.bowlingpoints = bowlingpoints;
	}
	public Map<String, String> getFieldingpoints() {
		return fieldingpoints;
	}
	public void setFieldingpoints(Map<String, String> fieldingpoints) {
		this.fieldingpoints = fieldingpoints;
	}
	public Map<String, String> getOtherpoints() {
		return otherpoints;
	}
	public void setOtherpoints(Map<String, String> otherpoints) {
		this.otherpoints = otherpoints;
	}
	
	
	
	
	
	

}
