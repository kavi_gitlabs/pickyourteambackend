package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Points;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")

public class PointsController {
	
	
	
	@Autowired
	private  Points points;

//	GET Points

	@GetMapping("/pointsdata")
	
	public ResponseEntity<Map<String,Map<String,String>>> getPointsData(){
		
		Map<String,Map<String,String>> pointsData = new HashMap<>();
		
		
		pointsData.put("bat", points.getBattingpoints());
		pointsData.put("bowl", points.getBowlingpoints());
		pointsData.put("field", points.getFieldingpoints());
		
		return ResponseEntity.ok(pointsData);
		
	}
	
	
	
	
	
	
	

}
