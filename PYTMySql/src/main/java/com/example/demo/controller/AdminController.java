package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Admin;
import com.example.demo.repositories.AdminRepository;
import com.example.demo.services.AdminService;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
public class AdminController {

	
	
	@Autowired
	AdminService as;

	@Autowired
	private AdminRepository ar;
	

	@PostMapping("/addAdmin")
	public Admin createOneTeam(@RequestBody Admin admin) {
		
		return as.createTeam(admin);	
}
	
	   @PostMapping("/login/admin")
	    public Admin loginDetails(@RequestBody Admin admin) {
	        List<Admin> allAdmins = ar.findAll();
	        for(Admin currentAdmin: allAdmins) {
	            if (currentAdmin.getUsername().equals(admin.getUsername())) {
	                if (currentAdmin.getPassword().equals(admin.getPassword())) {
	                    return currentAdmin;
	                }
	                return admin;
	            }
	        }
	        return admin;
	    }
	
	
	
	
	
}
