package com.example.demo.controller;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Points;
import com.example.demo.model.Users;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.UserService;

 

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
public class UserController {
	
	

	

	@Autowired
	UserService us;

	@Autowired
	private UserRepository ur;

	
	@Value("${pointsystem.battingpoints.runs}")
	private String baPoRuns;
	@Value("${pointsystem.battingpoints.boundary}")
	private String baPoBoundary;
	@Value("${pointsystem.battingpoints.six}")
	private String baPoSix;
	
	
	@Value("${pointsystem.bowlingpoints.wicket}")
	private String boPoWicket;
	@Value("${pointsystem.bowlingpoints.dot}")
	private String boPoDot;
	

	
	@Value("${pointsystem.fieldingpoints.boundary}")
	private String fiPoBoundary;
	@Value("${pointsystem.fieldingpoints.six}")
	private String fiPoSix;
	@Value("${pointsystem.fieldingpoints.catch}")
	private String fiPoCatch;
	
	@Value("${pointsystem.otherpoints.captain}")
	private String oTPoCap;
	
	@Value("${pointsystem.otherpoints.wicketKeeper}")
	private String oTPoWK;

	@Value("${appProp.name}")
	private String name;
	

	@Value("${rules.rule1}")
	private String rule1;

	@Value("${rules.rule2}")
	private String rule2;

	@Value("${rules.rule3}")
	private String rule3;
	

	@Value("${rules.rule4}")
	private String rule4;
	
//	public Points pointsData;
	

	
	

	
	@GetMapping("/matchpoint")
	public String getMatchPoints( ) {
		

		String pointsData = baPoRuns + '-'+ baPoBoundary + '-'+baPoSix+ '-'+ boPoWicket + '-'+ boPoDot + '-'+ fiPoBoundary+ '-'+fiPoSix + '-'+ fiPoCatch +  '-'+ oTPoCap + '-'+ oTPoWK + '-' +  name + '-' +  rule1 + '-' +  rule2+'-' +  rule3  + '-'+ rule4 ;
		
		return pointsData;		
}
	

	@PostMapping("/addUser")
	public Integer createOneTeam(@RequestBody Users user) {
		
		   List<Users> allUsers = ur.findAll();
		   
	        for(Users currentUser: allUsers) {
	            if (currentUser.getUsername().equals(user.getUsername())) {
	                return 0 ;
	            }
	        }
	        	us.createTeam(user);	
	        	return 1;
	        
}
	
	
	
	
//	GET users

	@GetMapping("/users")
	public List<Users> findAllUsers( ) {
		return us.getUsers();
}
	
	
	
	
	
//	GET team by ID
	@GetMapping("/users/{id}")
	public Users findUserbyId(@PathVariable int id) {
		
		return us.getUserbyId(id);
		
}
	
	
	
	
	
	
	
   
    @PostMapping("/login/user")
    public Users insertDetails(@RequestBody Users user) {
        List<Users> allUsers = ur.findAll();
        for(Users currentUser: allUsers) {
            if (currentUser.getUsername().equals(user.getUsername())) {
                if (currentUser.getPassword().equals(user.getPassword())) {
                    return currentUser;
                }
                return user;
            } 
        }
        return user;     
    } 
    

//	Edit User by ID
	@PutMapping("/editusers")
	public Users editPlayer (@RequestBody Users user) {
		
		 return us.editPlayer(user); 
		 
	}
	

//	Delete team by ID
@DeleteMapping("deleteuser/{id}")
public void deleteUser (@PathVariable int id) {
	
	  us.deleteUserbyId(id);
	 
}
    
}
