package com.example.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Admin;
import com.example.demo.model.Users;
import com.example.demo.repositories.AdminRepository;

@Service
public class AdminService {
	

	@Autowired
	private AdminRepository ar;
	

//	 POST new Admin (OK)
	public Admin createTeam(Admin admin) {
		return ar.save(admin);
	}
	
//	GET users by ID
	public Admin getAdminbyId(int id) {
		
		return ar.findById(id).orElse(null);
	}

//	GET teams
	public List<Admin> getAdmins() {
		return ar.findAll();
	}

}
