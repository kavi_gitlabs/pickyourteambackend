package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.model.Users;
import com.example.demo.repositories.UserRepository;


@Service  
public class UserService {
	
	@Autowired
	private UserRepository userRepo;

//	 POST new Team (OK)
	public Users createTeam(Users user) {
		return userRepo.save(user);
	}
	  

//	GET users by ID
	public Users getUserbyId(int id) {
		
		return userRepo.findById(id).orElse(null);
	}

//	GET teams
	public List<Users> getUsers() {
		return userRepo.findAll();
	}

	
	

//	Edit User by ID
	public Users editPlayer(@RequestBody Users user) {

		
		Optional<Users> currentUser = userRepo.findById(user.getId());
		
		currentUser.get().setFirstname(user.getFirstname());
		currentUser.get().setLastname(user.getLastname());
		currentUser.get().setUsername(user.getUsername());
		currentUser.get().setPassword(user.getPassword());
		currentUser.get().setEmail(user.getEmail());
		currentUser.get().setMobile(user.getMobile());
		
		return userRepo.save(currentUser.get());
	}
	
	
	//Delete by Id 
	
	public String deleteUserbyId(int id) {
		userRepo.deleteById(id);
		return "User removed " +id ;
	}

}
