package com.example.demo;

import java.util.List;

public class UserContestData {
	
	private String contestid;
	private String userid;
	private String matchid;
	private List<UserPlayerData> userPlayerData;
	private String isCaptain;
	private String isWicketKeeper;
	
	

	public String getContestid() {
		return contestid;
	}
	public void setContestid(String contestid) {
		this.contestid = contestid;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getMatchid() {
		return matchid;
	}
	public void setMatchid(String matchid) {
		this.matchid = matchid;
	}
	public List<UserPlayerData> getUserPlayerData() {
		return userPlayerData;
	}
	public void setUserPlayerData(List<UserPlayerData> userPlayerData) {
		this.userPlayerData = userPlayerData;
	}
	public String getIsCaptain() {
		return isCaptain;
	}
	public void setIsCaptain(String isCaptain) {
		this.isCaptain = isCaptain;
	}
	public String getIsWicketKeeper() {
		return isWicketKeeper;
	}
	public void setIsWicketKeeper(String isWicketKeeper) {
		this.isWicketKeeper = isWicketKeeper;
	}
	
	
	
	
	
	

}
