package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
 
 


public class mapContestController {
	
	

	@Autowired
	mapContestService ms;
	
	@Autowired

	mapContestRepository mr;
	
	
	@GetMapping("/")
	public String Firstapp() {
		return "Hello from the heaven";
		
	}
	
//	 POST new map contest
	
	
	
	@PostMapping("/mapcontest")
	public Integer createContest(@RequestBody mapContest contest) {
		
		
		List<mapContest> allMappedContests = mr.findAll();
		
	    for(mapContest currentUser: allMappedContests) {
	    	
	    	if (currentUser.getContestid().equals(contest.getContestid() ) 
	    			&& currentUser.getMatchid().equals(contest.getMatchid()) ) {
	    		return 0;
	    	}
	    }
		
		 ms.createmapContest(contest);	
		 return 1;
	}
	
	
	
	
	
	
//	GET All Contests
	
	@GetMapping("/mappedcontests")
	public List<mapContest> findAllmappedData( ) {
		return ms.getContests();
}
	
	


//	Delete contest by ID
@DeleteMapping("deletemappedcontest/{id}")
public void deleteContest (@PathVariable int id) {
	
	  ms.deletemappedContestbyId(id);
	 
}	


@GetMapping("/mappedcontests/{id}")
public List<mapContest> findAllmappedDataBymatchid( @PathVariable int id) {
	
	
	return ms.findByName(id);
}


} 
	
	
	

 