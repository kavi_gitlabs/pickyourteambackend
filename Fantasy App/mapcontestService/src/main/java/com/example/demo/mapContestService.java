package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class mapContestService {

	
	@Autowired
	private mapContestRepository mapRepo;
	
	
	public mapContest createmapContest(mapContest mapcontest) {
		return mapRepo.save(mapcontest);
	}
	

//	GET mappedContests
	public List<mapContest> getContests() {
		return mapRepo.findAll();
	}
	
	

//	GET mappedContests
	public List<mapContest> getContestsbyid() {
		return mapRepo.findAll();
	}
	

//	GET mappedContest by ID
	public mapContest getContestbyId(int id) {
		
		return mapRepo.findById(id).orElse(null);
	}
	
	
	
//	GET mappedContest by ID
	public List<mapContest> findByName(int mapid) {
		
	System.out.println(mapid + "Hai from match id");
		return mapRepo.findByName(mapid);
		
		
	}

	
	//Delete by Id 
	
	public String deletemappedContestbyId(int id) {
		System.out.println(id + "Hai from delete match id");

		mapRepo.deleteById(id);
		return "mappedContest removed " + id ;
	}
	
	
	
	
	
	
}
