package com.example.demo;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;




@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
public class TeamController {
	

	
	
	@GetMapping("/")
	public String Firstapp() {
		return "R u looking 4 me";
	}
	
	
	
//	 POST new Team

	@Autowired
	TeamService servicess;
	
	
	@PostMapping("/addTeam")
	public Team createOneTeam(@RequestBody Team team) {
		
		return servicess.createTeam(team);	
}
	
//	GET teams

	@GetMapping("/teams")
	public List<Team> findAllTeams( ) {
		return servicess.getTeams();	
}
	
	
//	GET team by ID
	@GetMapping("/teams/{id}")
	public Team findTeambyId(@PathVariable int id) {
		
		return servicess.getTeambyId(id);	
}
	
	
//	GET team by Name
//	@GetMapping("/teams/{name}")
//	public Team findTeambyName(@PathVariable String  name) {
//		
//		return servicess.getTeambyName(name);	
//}
	
//	Edit team by ID
	@PutMapping("/editteam")
	public Team editTeam (@RequestBody Team team) {
		
		return servicess.editTeam(team);
		
	}
	
//	DELETE team by ID
	
	@DeleteMapping("/deleteteam/{id}")
	public void deleteTeam (@PathVariable int id) {		
		 servicess.deleteTeambyId(id);
	}

	

}

















