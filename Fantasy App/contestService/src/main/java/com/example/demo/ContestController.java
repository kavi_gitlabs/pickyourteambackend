package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
 


public class ContestController {

	@Autowired
	ContestService cs;
	
	
	@GetMapping("/")
	public String Firstapp() {
		return "R u looking 4 me";
	}
	
	
	
//	 POST new Fixture contest
	
	@PostMapping("/addContest")
	public Contest createContest(@RequestBody Contest contest) {
		
		return cs.createContest(contest);	
	}
	
	
//	GET All Contests
	
	@GetMapping("/contest")
	public List<Contest> findAllContests( ) {
		return cs.getContests();
}
	
	

	
//	GET contest by new ID
	@GetMapping("/contests/{id}")
	public Contest findContestbyId(@PathVariable int id) {
		
		return cs.getContestbyId(id);
}
	
	
//	Edit contest by ID
	
	@PutMapping("/editcontest")
	public Contest editContest(@RequestBody Contest contest) {
		
		 return cs.editcontest(contest);
	}
	
	

//	Delete contest by ID
@DeleteMapping("deletecontest/{id}")
public void deleteContest (@PathVariable int id) {
	
	  cs.deleteContestbyId(id);
	 
}
	
	
}
