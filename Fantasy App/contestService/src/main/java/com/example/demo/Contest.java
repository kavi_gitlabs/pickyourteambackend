package com.example.demo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;




@Entity
@Table  (name = "contests")

public class Contest {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; 
	
	
	@Column(name="contestname")
	private String contestname;
	
	
	@Column(name="maxsize")
	private String maxsize;
	
	@Column(name="minsize")
	private String minsize;
	
	@Column(name="prizemoney")
	private String prizemoney;
	
	@Column(name="entryfee")
	private String entryfee;
	
	@Column(name="rank1")
	private String rank1;
	
	@Column(name="rank2")
	private String rank2;
	
	@Column(name="rank3")
	private String rank3;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContestname() {
		return contestname;
	}

	public void setContestname(String contestname) {
		this.contestname = contestname;
	}

	public String getMaxsize() {
		return maxsize;
	}

	public void setMaxsize(String maxsize) {
		this.maxsize = maxsize;
	}

	public String getMinsize() {
		return minsize;
	}

	public void setMinsize(String minsize) {
		this.minsize = minsize;
	}

	public String getPrizemoney() {
		return prizemoney;
	}

	public void setPrizemoney(String prizemoney) {
		this.prizemoney = prizemoney;
	}

	public String getEntryfee() {
		return entryfee;
	}

	public void setEntryfee(String entryfee) {
		this.entryfee = entryfee;
	}

	public String getRank1() {
		return rank1;
	}

	public void setRank1(String rank1) {
		this.rank1 = rank1;
	}

	public String getRank2() {
		return rank2;
	}

	public void setRank2(String rank2) {
		this.rank2 = rank2;
	}

	public String getRank3() {
		return rank3;
	}

	public void setRank3(String rank3) {
		this.rank3 = rank3;
	}
	
	
	

}
