package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins="*")
@RestController
@RequestMapping("/")
 


public class FixtureController {

	@Autowired
	FixtureService fs;
	
	
	@GetMapping("/")
	public String Firstapp() {
		return "R u looking 4 me";
	}
	
	
//	 POST new Fixture
	
	@PostMapping("/addFixture")
	public Fixture createFixture(@RequestBody Fixture fixture) {
		
		return fs.createFixture(fixture);	
	}
	
//	GET All Players
	
	@GetMapping("/fixtures")
	public List<Fixture> findAllFixtures( ) {
		return fs.getFixtures();
}
	
	
	
//	GET team by ID
	@GetMapping("/fixtures/{id}")
	public Fixture findPlayerbyId(@PathVariable int id) {
		
		return fs.getfixturebyId(id);	
}
	
	
	
//	Edit team by ID
	@PutMapping("/editfixture")
	public Fixture editFixture(@RequestBody Fixture fixture) {
		
		 return fs.editfixture(fixture);
		 
	}

//	Delete team by ID
@DeleteMapping("deletefixture/{id}")
public void deletePlayer (@PathVariable int id) {
	
	  fs.deleteFixturebyId(id);
	 
}
	
	
	
}
