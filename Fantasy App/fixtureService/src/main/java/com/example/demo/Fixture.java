package com.example.demo;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

// fixture 
	@Entity 
	@Table  (name = "fixtures")
	public class Fixture {  
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id; 
	
	
	@Column(name="team1") 
	private String team1;
	
	@Column(name="team2")
	private String team2;
	
	@Column(name="datetime")
	private String datetime;
	
	@Column(name="venue")
	private String venue;
	@Column(name="stage")
	private String stage;

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTeam1() {
		return team1;
	}

	public void setTeam1(String team1) {
		this.team1 = team1;
	}

	public String getTeam2() {
		return team2;
	}

	public void setTeam2(String team2) {
		this.team2 = team2;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}
	
	
	
	
	

}
