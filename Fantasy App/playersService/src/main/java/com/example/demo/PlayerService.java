package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;


@Service
public class PlayerService {
	
	
	@Autowired
	private PlayerRepository playerRepo;
	
	 
//	 POST new Player (OK)
	public Player createPlayer(Player player) {
		
		return playerRepo.save(player);
	}
	

//	GET players
	public List<Player> getPlayers() {
		return playerRepo.findAll();
	}
	

//	GET player by ID
	public Player getplayerbyId(int id) {
		
		return playerRepo.findById(id).orElse(null);
	}
	
	
	

//	Edit team by ID
	public Player editPlayer(@RequestBody Player player) {

		
		Optional<Player> currentTeam = playerRepo.findById(player.getId());
		
		currentTeam.get().setName(player.getName());
		currentTeam.get().setTeam_name(player.getTeam_name());
		currentTeam.get().setSkills(player.getSkills());
		currentTeam.get().setPoints(player.getPoints());
		
		return playerRepo.save(currentTeam.get());
	}
	
	
	//Delete by Id 
	
	public String deletePlayerbyId(int id) {
		playerRepo.deleteById(id);
		return "Team removed " +id ;
	}

	
	
	
	
	
	
}
